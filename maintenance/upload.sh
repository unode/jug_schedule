#!/usr/bin/env bash
# upload.sh -- created 2017-03-21 - Renato Alves

source venv3/bin/activate
pip install --quiet twine

twine upload dist/*
