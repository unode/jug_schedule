#!/usr/bin/env bash
# release.sh -- created 2017-03-21 - Renato Alves

set -e

case "$1" in
    fix)
        echo "Doing a '$1' release"
        ;;
    minor)
        echo "Doing a '$1' release"
        ;;
    major)
        echo "Doing a '$1' release"
        ;;
    *)
        echo "Ignoring release request. Need one of:"
        echo "  fix   -> bugfix release (e.g 0.2.1 -> 0.2.2)"
        echo "  minor -> minor release  (e.g 0.2.1 -> 0.3.0)"
        echo "  major -> major release  (e.g 0.2.1 -> 1.0.0)"
        exit 1
        ;;
esac

maintenance/bumpversion.py "$1"
maintenance/package.sh
maintenance/bumpversion.py --add-git-tag "tag"
maintenance/upload.sh
