#!/usr/bin/env python3
# bumpversion.py -- created 2017-03-21 - Renato Alves

import argparse
from subprocess import check_call


class Version:
    def __init__(self, version, args):
        self.args = args
        self.tail = ''
        if "+" in version:
            main, self.tail = version.split("+")
        else:
            main = version

        self.major, self.minor, self.fix = map(int, main.split('.'))

    def bump(self):
        func_target = "bump_{}".format(self.args.target)
        action = getattr(self, func_target)
        action()

    def bump_major(self):
        self.major += 1
        self.minor = 0
        self.fix = 0
        self.tail = ''

    def bump_minor(self):
        self.minor += 1
        self.fix = 0
        self.tail = ''

    def bump_fix(self):
        self.fix += 1
        self.tail = ''

    def bump_tag(self):
        # Tag git repository with current version
        self.tail = ''
        version = str(self)
        self.tail = "+git"

        if self.args.add_git_tag:
            save_version(self.args, self)
            check_call(["git", "add", self.args.version_file])
            check_call(["git", "commit", "-m", "REL Release version {}".format(version)])
            check_call(["git", "tag", version])

    def __str__(self):
        return "{}.{}.{}{}".format(self.major, self.minor, self.fix, self.tail)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("target", action="store", choices=["major", "minor", "fix", "tag"])
    parser.add_argument("-f", "--version-file", action="store", default="jug_schedule/version.py")
    parser.add_argument("-n", "--dryrun", action="store_true")
    parser.add_argument("-t", "--add-git-tag", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")

    return parser.parse_args()


def save_version(args, version):
    with open(args.version_file, 'w') as fh:
        fh.write("__version__ = '{}'\n".format(version))


def get_version(args):
    with open(args.version_file) as fh:
        data = fh.read()

    _, version, _ = data.split("'")

    return Version(version, args)


def main():
    args = parse_args()
    version = get_version(args)

    before = str(version)
    version.bump()
    after = str(version)

    if args.verbose or args.dryrun:
        print("Bumped:", before, "->", after)

    if not args.dryrun:
        save_version(args, version)


if __name__ == "__main__":
    main()
