#!/usr/bin/env bash
# package.sh -- created 2017-03-21 - Renato Alves

rm -rf dist
rm -rf build

if [ ! -d "venv3" ]; then
    virtualenv -p python3 venv3
else
    echo "Skipping virtualenv creation. 'venv3' already exists"
fi
source venv3/bin/activate
python setup.py --quiet sdist bdist_wheel --universal
