from .resources import ResourcesTaskGenerator, GE, SGE, SLURM, LSF
from .queues import identify_system
